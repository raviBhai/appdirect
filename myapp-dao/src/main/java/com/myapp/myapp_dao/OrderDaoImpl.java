package com.myapp.myapp_dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.myapp.myapp_dao.entity.Order;

@Repository
public class OrderDaoImpl extends BaseDaoImpl<Order> implements OrderDao {
	
	public OrderDaoImpl() {
		super(Order.class);
	}
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	QueryDao queryDao;
	
	@PersistenceContext
	EntityManager entityManager;
	
}
