package com.myapp.myapp_dao;

import com.myapp.myapp_dao.entity.Order;

public interface OrderDao extends BaseDao<Order> {

}
