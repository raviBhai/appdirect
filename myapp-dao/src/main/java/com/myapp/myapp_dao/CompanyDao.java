package com.myapp.myapp_dao;

import com.myapp.myapp_dao.entity.Company;

public interface CompanyDao extends BaseDao<Company> {
	public Company findByUuid(String uuid);
}
