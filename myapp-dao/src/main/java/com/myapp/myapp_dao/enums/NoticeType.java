package com.myapp.myapp_dao.enums;

public enum NoticeType {
	REACTIVATED, 
	DEACTIVATED, 
	CLOSED
}
