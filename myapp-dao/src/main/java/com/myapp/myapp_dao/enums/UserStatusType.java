package com.myapp.myapp_dao.enums;


public enum UserStatusType {

    ACTIVE,
    BLOCKED,
    DELETED

}
