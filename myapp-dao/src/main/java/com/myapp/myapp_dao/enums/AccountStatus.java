package com.myapp.myapp_dao.enums;

public enum AccountStatus {
	INITIALIZED,
	FAILED, 
	FREE_TRIAL, 
	FREE_TRIAL_EXPIRED, 
	ACTIVE, 
	SUSPENDED, 
	CANCELLED
}
