package com.myapp.myapp_dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.myapp.myapp_dao.entity.Account;
import com.myapp.myapp_dao.util.CommonUtil;
import com.myapp.myapp_dao.util.LazyIterator;

@Repository
public class AccountDaoImpl extends BaseDaoImpl<Account> implements AccountDao {
	
	public AccountDaoImpl() {
		super(Account.class);
	}
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	QueryDao queryDao;
	
	@PersistenceContext
	EntityManager entityManager;

	public Account findByUuid(String accountIdentifier) {
		List<Account> accountList = entityManager.createQuery("from " + Account.class.getName()
				+ " where accountIdentifier = :accountIdentifier ", Account.class)
				.setParameter("accountIdentifier", accountIdentifier)
				.getResultList();
		if(CommonUtil.notNullAndEmpty(accountList)) {
			return accountList.get(0);
		}
		return null;
	}
	
	public Account findByUuidWithAllUsers(String accountIdentifier) {
		List<Account> accountList = entityManager.createQuery("from " + Account.class.getName()
				+ " where accountIdentifier = :accountIdentifier ", Account.class)
				.setParameter("accountIdentifier", accountIdentifier)
				.getResultList();
		if(CommonUtil.notNullAndEmpty(accountList)) {
			Account account = accountList.get(0);
			if(CommonUtil.isNotNull(account.getUsers())){
				LazyIterator.iter(account.getUsers());
			}
			return account;
		}
		return null;
	}

}
