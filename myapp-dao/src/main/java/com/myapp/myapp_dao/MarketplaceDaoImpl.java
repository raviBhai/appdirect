package com.myapp.myapp_dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.myapp.myapp_dao.entity.Marketplace;

@Repository
public class MarketplaceDaoImpl extends BaseDaoImpl<Marketplace> implements MarketplaceDao {
	
	public MarketplaceDaoImpl() {
		super(Marketplace.class);
	}
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	QueryDao queryDao;
	
	@PersistenceContext
	EntityManager entityManager;
	
}
