package com.myapp.myapp_dao.common;

public class MyAppConstants {
	public static final String SUCCESS = "success";
	public static final String ACCOUNT_IDENTIFIER = "accountIdentifier";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String ERROR_CODE = "errorCode";
	public static final String MESSAGE = "message";
	
	public static final String FAILED = "FAILED";
	public static final String STATUS = "status";
	public static final String RESULT = "result";
	
	public static final String OAUTH_NONCE = "oauth_nonce";
	public static final String OAUTH_TIMESTAMP = "oauth_timestamp";
	public static final String OAUTH_CONSUMER_KEY = "oauth_consumer_key";
	public static final String OAUTH_SIGNATURE_METHOD = "oauth_signature_method";
	public static final String OAUTH_VERSION = "oauth_version";
	public static final String OAUTH_SIGNATURE = "oauth_signature";
	
	public static final String EQUALS_TO = "=";
	public static final String COMMA = ",";
	public static final String EMPTY_STRING = "";
	
	public static final String OAUTH_CONSUMER_PUBLIC_KEY = "oauth-consumer-public-key";
	public static final String OAUTH_CONSUMER_SECRET_KEY = "oauth-consumer-private-key";

}
