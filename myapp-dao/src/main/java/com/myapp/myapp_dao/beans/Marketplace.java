package com.myapp.myapp_dao.beans;

public class Marketplace {
	
	private Integer marketPlaceId;
	private String partner;
	private String baseUrl;
	
	public Integer getMarketPlaceId() {
		return marketPlaceId;
	}
	public void setMarketPlaceId(Integer marketPlaceId) {
		this.marketPlaceId = marketPlaceId;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	
}
