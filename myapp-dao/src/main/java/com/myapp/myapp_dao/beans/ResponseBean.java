package com.myapp.myapp_dao.beans;

import com.myapp.myapp_dao.enums.Flag;
import com.myapp.myapp_dao.enums.SubscriptionType;

public class ResponseBean {
	private SubscriptionType type;
	private Marketplace marketplace;
	private String applicationUuid;
	private Flag flag;
	private String returnUrl;
	private User creator;
	private Payload payload;
	private Object[] links;
	
	public SubscriptionType getType() {
		return type;
	}
	public void setType(SubscriptionType type) {
		this.type = type;
	}
	public Marketplace getMarketplace() {
		return marketplace;
	}
	public void setMarketplace(Marketplace marketplace) {
		this.marketplace = marketplace;
	}
	public String getApplicationUuid() {
		return applicationUuid;
	}
	public void setApplicationUuid(String applicationUuid) {
		this.applicationUuid = applicationUuid;
	}
	public Flag getFlag() {
		return flag;
	}
	public void setFlag(Flag flag) {
		this.flag = flag;
	}
	public String getReturnUrl() {
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public Payload getPayload() {
		return payload;
	}
	public void setPayload(Payload payload) {
		this.payload = payload;
	}
	public Object[] getLinks() {
		return links;
	}
	public void setLinks(Object[] links) {
		this.links = links;
	}
}
