package com.myapp.myapp_dao.entity;

import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.util.CommonUtil;

@Entity
@Table(name = "market_place")
public class Marketplace extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = AUTO)
	@Column
	private int id;
	
	@Column
	private String partnelUrl;
	@Column
	private String baseUrl;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPartnelUrl() {
		return partnelUrl;
	}
	public void setPartnelUrl(String partnelUrl) {
		this.partnelUrl = partnelUrl;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public static Marketplace getMarketplaceFromResponseBean(ResponseBean responseBean){
		Marketplace marketplace = null;
		if(CommonUtil.isNotNull(responseBean)){
			com.myapp.myapp_dao.beans.Marketplace marketPlaceBean = responseBean.getMarketplace();
			if(CommonUtil.isNotNull(marketPlaceBean)){
				marketplace = new Marketplace();
				marketplace.setPartnelUrl(marketPlaceBean.getPartner());
				marketplace.setBaseUrl(marketPlaceBean.getBaseUrl());
			}
		}
		return marketplace;
	}
}
