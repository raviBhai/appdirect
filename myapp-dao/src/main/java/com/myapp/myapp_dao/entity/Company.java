package com.myapp.myapp_dao.entity;

import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.myapp.myapp_dao.beans.Payload;
import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.util.CommonUtil;

@Entity
@Table(name = "company")
public class Company extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = AUTO)
	@Column
	private int id;
	
	@Column(unique = true)
	private String uuid;
	@Column
	private String externalId;
	@Column
	private String name;
	@Column
	private String email;
	@Column
	private String phoneNumber;
	@Column
	private String website;
	@Column
	private String country;
	
	@OneToOne(mappedBy = "company")
	private Account account;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	public static Company getCompanyFromResponseBean(ResponseBean responseBean){
		Company company = null;
		if(CommonUtil.isNotNull(responseBean)){
			Payload payload = responseBean.getPayload();
			com.myapp.myapp_dao.beans.Company companyBean = payload.getCompany();
			if(CommonUtil.isNotNull(companyBean)){
				company = new Company();
				company.setUuid(companyBean.getUuid());
				company.setExternalId(companyBean.getExternalId());
				company.setName(companyBean.getName());
				company.setEmail(companyBean.getEmail());
				company.setPhoneNumber(companyBean.getPhoneNumber());
				company.setWebsite(companyBean.getWebsite());
				company.setCountry(companyBean.getCountry());
			}
		}
		return company;
	}
	
	public static String getUuidFromResponseBean(ResponseBean responseBean){
		String uuid = null;
		if(CommonUtil.isNotNull(responseBean)){
			Payload payload = responseBean.getPayload();
			if(CommonUtil.isNotNull(payload)){
				com.myapp.myapp_dao.beans.Company company = payload.getCompany();
				if(CommonUtil.isNotNull(company)){
					uuid = company.getUuid();
				}
			}
		}
		return uuid;
	}
	
}
