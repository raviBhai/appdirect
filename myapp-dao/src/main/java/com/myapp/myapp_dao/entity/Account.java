package com.myapp.myapp_dao.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.myapp.myapp_dao.beans.Payload;
import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.enums.AccountStatus;
import com.myapp.myapp_dao.util.CommonUtil;

@Entity
@Table(name = "account")
public class Account extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = AUTO)
	@Column
	private int id;
	
	@Column(unique = true)
	private String accountIdentifier;
	
	@Enumerated(EnumType.STRING)
	@Column
	private AccountStatus status;
	
	@OneToOne
	@JoinColumn(name = "created_in_market_place_id", nullable = false)
	private Marketplace marketplace;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "company_id", nullable = false)
	private Company company;
	
	@OneToOne
	@JoinColumn(name = "admin_id")
	private User adminUser;
	
	@OneToMany(mappedBy = "account")
	private List<User> users;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Marketplace getMarketplace() {
		return marketplace;
	}

	public void setMarketplace(Marketplace marketplace) {
		this.marketplace = marketplace;
	}
	
	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Company getCompany() {
		return company;
	}
	
	public User getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(User adminUser) {
		this.adminUser = adminUser;
	}

	public void addUser(User user){
		users.add(user);
	}
	
	public static Account createNewAccount(){
		Account account = new Account();
		account.setAccountIdentifier(UUID.randomUUID().toString());
		account.setStatus(AccountStatus.INITIALIZED);
		return account;
	}
	
	public static String getAccountIdentifierFromResponseBean(ResponseBean responseBean){
		String accountIdentifier = null;
		if(CommonUtil.isNotNull(responseBean)){
			Payload payload = responseBean.getPayload();
			if(CommonUtil.isNotNull(payload)){
				com.myapp.myapp_dao.beans.Account account = payload.getAccount();
				if(CommonUtil.isNotNull(account)){
					accountIdentifier = account.getAccountIdentifier();
				}
			}
		}
		return accountIdentifier;
	}
}
