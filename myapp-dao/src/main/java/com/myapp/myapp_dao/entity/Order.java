package com.myapp.myapp_dao.entity;

import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.myapp.myapp_dao.beans.Payload;
import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.util.CommonUtil;

@Entity
@Table(name = "orders")
public class Order extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = AUTO)
	@Column
	private int id;
	
	@Column
	private String editionCode;
	@Column
	private String addonOfferingCode;
	@Column
	private String pricingDuration;
	@Column
	private String quantity;
	@Column
	private String unit;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "creator_id")
	private User creator;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEditionCode() {
		return editionCode;
	}

	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}

	public String getAddonOfferingCode() {
		return addonOfferingCode;
	}

	public void setAddonOfferingCode(String addonOfferingCode) {
		this.addonOfferingCode = addonOfferingCode;
	}

	public String getPricingDuration() {
		return pricingDuration;
	}

	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}
	
	public static Order getOrderFromResponseBean(ResponseBean responseBean){
		Order order = null;
		if(CommonUtil.isNotNull(responseBean)){
			Payload payload = responseBean.getPayload();
			if(CommonUtil.isNotNull(payload)){
				com.myapp.myapp_dao.beans.Order orderBean = payload.getOrder();
				if(CommonUtil.isNotNull(orderBean)){
					order = new Order();
					order.setEditionCode(orderBean.getEditionCode());
					order.setAddonOfferingCode(orderBean.getAddonOfferingCode());
					order.setPricingDuration(orderBean.getPricingDuration());
					/*Item item = orderBean.getItem();
					if(CommonUtil.isNotNull(item)){
						order.setQuantity(item.getQuantity());
						order.setUnit(item.getUnit());
					}*/
				}
			}
		}
		return order;		
	}
}
