package com.myapp.myapp_dao.entity;

import static javax.persistence.GenerationType.AUTO;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.myapp.myapp_dao.beans.Address;
import com.myapp.myapp_dao.beans.Payload;
import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.enums.UserStatusType;
import com.myapp.myapp_dao.util.CommonUtil;

@Entity
@Table(name = "user")
public class User extends BaseEntity{

	private static final long serialVersionUID = -5464369837316338488L;

	@Id
	@GeneratedValue(strategy = AUTO)
	@Column
	private int id;

	
	@Column
	private String salutation;
	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private String fullName;
	@Column
	private String companyName;
	
	@Column
	private String phone;
	@Column
	private String phoneExtension;
	@Column
	private String fax;
	@Column
	private String faxExtension;
	
	@Column
	private String language;
	@Column
	private String openId;
	@Column(unique = true)
	private String uuid;
	
	@Column
	private String city;
	@Column
	private String country;
	@Column
	private String state;
	@Column
	private String street1;
	@Column
	private String street2;
	@Column
	private String zip;
	
	@Column
	private String pobox;
	@Column
	private String pozip;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "account_id", nullable = false)
	private Account account;
	
	@Enumerated(EnumType.STRING)
	@Column
	private UserStatusType status;
	
	@OneToOne
	@JoinColumn(name = "creator_id", nullable = true)
	private User creator;
	
	@OneToMany(mappedBy = "creator")
	private List<Order> orders;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public UserStatusType getStatus() {
		return status;
	}

	public void setStatus(UserStatusType status) {
		this.status = status;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}
	
	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhoneExtension() {
		return phoneExtension;
	}

	public void setPhoneExtension(String phoneExtension) {
		this.phoneExtension = phoneExtension;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFaxExtension() {
		return faxExtension;
	}

	public void setFaxExtension(String faxExtension) {
		this.faxExtension = faxExtension;
	}

	public String getPobox() {
		return pobox;
	}

	public void setPobox(String pobox) {
		this.pobox = pobox;
	}

	public String getPozip() {
		return pozip;
	}

	public void setPozip(String pozip) {
		this.pozip = pozip;
	}
	
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public static String getCreatorUuidFromResponseBean(ResponseBean responseBean){
		String uuid = null;
		if(CommonUtil.isNotNull(responseBean)){
			com.myapp.myapp_dao.beans.User creator = responseBean.getCreator();
			if(CommonUtil.isNotNull(creator)){
				uuid = creator.getUuid();
			}
		}
		return uuid;
	}
	
	public static User getCreatorUserFromResponseBean(ResponseBean responseBean){
		User user = null;
		if(CommonUtil.isNotNull(responseBean)){
			com.myapp.myapp_dao.beans.User creator = responseBean.getCreator();
			if(CommonUtil.isNotNull(creator)){
				user = populateUserFromBean(creator);
			}
		}
		return user;
	}
	
	public static User getPayloadUserFromResponseBean(ResponseBean responseBean){
		User user = null;
		if(CommonUtil.isNotNull(responseBean)){
			Payload payload = responseBean.getPayload();
			if(CommonUtil.isNotNull(payload)){
				com.myapp.myapp_dao.beans.User payloadUser = payload.getUser();
				if(CommonUtil.isNotNull(payloadUser)){
					user = populateUserFromBean(payloadUser);
				}
			}
		}
		return user;
	}

	private static User populateUserFromBean(com.myapp.myapp_dao.beans.User creator) {
		User user = new User();
		user.setFirstName(creator.getFirstName());
		user.setLastName(creator.getLastName());
		user.setLanguage(creator.getLanguage());
		user.setOpenId(creator.getOpenId());
		user.setUuid(creator.getUuid());
		Address address = creator.getAddress();
		if(CommonUtil.isNotNull(address)){
			user.setSalutation(address.getSalutation());
			user.setFullName(address.getFullName());
			user.setCompanyName(address.getCompanyName());
			user.setPhone(address.getPhone());
			user.setPhoneExtension(address.getPhoneExtension());
			user.setFax(address.getFax());
			user.setFaxExtension(address.getFaxExtension());
			user.setCity(address.getCity());
			user.setCountry(address.getCountry());
			user.setState(address.getState());
			user.setStreet1(address.getStreet1());
			user.setStreet2(address.getStreet2());
			user.setZip(address.getZip());
			user.setPobox(address.getPobox());
			user.setPozip(address.getPozip());
		}
		user.setStatus(UserStatusType.ACTIVE);
		user.setCreator(null);
		return user;
	}
	
}
