package com.myapp.myapp_dao;

import com.myapp.myapp_dao.entity.Account;

public interface AccountDao extends BaseDao<Account> {
	
		public Account findByUuid(String accountIdentifier);
		
		public Account findByUuidWithAllUsers(String accountIdentifier);
}
