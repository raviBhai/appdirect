package com.myapp.myapp_dao;

import com.myapp.myapp_dao.entity.Marketplace;

public interface MarketplaceDao extends BaseDao<Marketplace> {

}
