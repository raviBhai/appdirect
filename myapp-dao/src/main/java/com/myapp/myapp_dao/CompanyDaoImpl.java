package com.myapp.myapp_dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.myapp.myapp_dao.entity.Company;
import com.myapp.myapp_dao.util.CommonUtil;

@Repository
public class CompanyDaoImpl extends BaseDaoImpl<Company> implements CompanyDao {
	
	public CompanyDaoImpl() {
		super(Company.class);
	}
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	QueryDao queryDao;
	
	@PersistenceContext
	EntityManager entityManager;

	public Company findByUuid(String uuid) {
		List<Company> companyList = entityManager.createQuery("from " + Company.class.getName()
				+ " where uuid = :uuid ", Company.class)
				.setParameter("uuid", uuid)
				.getResultList();
		if(CommonUtil.notNullAndEmpty(companyList)) {
			return companyList.get(0);
		}
		return null;
	}
	
}
