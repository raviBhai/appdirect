package com.myapp.myapp_dao.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropertyLoader {
	
	static final Logger log = LogManager.getLogger(PropertyLoader.class.getName());
	
	private static final PropertyLoader instance;
	
	private Properties properties = new Properties();
	static {
		 instance = new PropertyLoader();
	}
	private PropertyLoader() {
		try {
			InputStream inputStream = PropertyLoader.class.getClassLoader().getResourceAsStream("myapp.properties");
			properties.load(inputStream);
		}catch(IOException e){
			log.error("Error while loading properties");
		}
	}
	
	public static PropertyLoader getInstance(){
		return instance;
	}
	
	public String getValue(String key){
		return properties.getProperty(key);
	}
}
