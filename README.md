* Install Java 8 and maven on your machine.
* Check out the code on your local directory and navigate to /appdirect/myapp
* Run mvn clean install to build the project
* .war file will be created under /appdirect/myapp-web/target/myapp-web.war
* Deploy it on Tomcat 8 to run the application 