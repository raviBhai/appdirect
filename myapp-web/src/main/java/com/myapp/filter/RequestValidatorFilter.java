package com.myapp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.myapp.util.OAuthSignatureValidator;

public class RequestValidatorFilter implements Filter {

	static final Logger log = LogManager.getLogger(RequestValidatorFilter.class.getName());
	
	public void destroy() {
		log.info("destroy method called");
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		log.info("init method called");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException {
		try {
			/*
			log.info("doFilter :: About to oauth verify request");
			if(OAuthSignatureValidator.isValidOAuthRequest(request)){
				log.info("doFilter :: Valid oauth request");
				chain.doFilter(request, response);
			}else{
				log.info("doFilter :: Invalid oauth request");
				request.setAttribute("errorMessage", "Invalid OAuth Request");
				request.getRequestDispatcher("/error/invalidOAuth").forward(request, response);
			}
			log.info("doFilter :: Request is oauth verified");
			*/
			chain.doFilter(request, response);
		} catch (Exception ex) {
			log.info("doFilter :: Exception occured while filtering request");
			request.setAttribute("errorMessage", ex);
			request.getRequestDispatcher("/error/otherError").forward(request, response);
		}

	}

}