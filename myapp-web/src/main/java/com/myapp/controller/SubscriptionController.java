package com.myapp.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_service.SubscriptionService;

@Controller
public class SubscriptionController {

	static final Logger log = LogManager.getLogger(SubscriptionController.class.getName());
	
	@Autowired
	SubscriptionService subscriptionService;
	
	@RequestMapping(value = "/createSubscription", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> createSubscription(@RequestParam("eventUrl") String eventUrl, HttpServletRequest request) {
    	log.info("createSubscription called with eventUrl - {}", eventUrl);
    	return subscriptionService.createSubscription(eventUrl, request);
    }
	
	@RequestMapping(value = "/cancelSubscription", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> cancelSubscription(@RequestParam("eventUrl") String eventUrl, HttpServletRequest request) {
		log.info("cancelSubscription called with eventUrl - {}", eventUrl);
    	return subscriptionService.cancelSubscription(eventUrl, request);
    }
	
	@RequestMapping(value = "/createSubscriptionWithJson", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> createSubscriptionWithJson(@RequestBody ResponseBean responseBean, HttpServletRequest request) {
		return subscriptionService.createSubscriptionWithJson(responseBean, request);
    }
	
	@RequestMapping(value = "/cancelSubscriptionWithJson", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> cancelSubscriptionWithJson(@RequestBody ResponseBean responseBean, HttpServletRequest request) {
		return subscriptionService.cancelSubscriptionWithJson(responseBean, request);
    }
}
