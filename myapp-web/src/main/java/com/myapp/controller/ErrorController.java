package com.myapp.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myapp.myapp_dao.common.ErrorCodes;
import com.myapp.myapp_dao.common.MyAppConstants;

@Controller
@RequestMapping(value = "/error")
public class ErrorController {

	static final Logger log = LogManager.getLogger(ErrorController.class.getName());
	
    @RequestMapping(value = "/invalidOAuth", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> errorGet(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	log.info("OAuth validation failed ");
    	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    	result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
		result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNAUTHORIZED);
		result.put(MyAppConstants.MESSAGE, "Invalid OAuth Request");
    	return result;
    }
    
    @RequestMapping(value = "/invalidOAuth", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> errorPost(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	log.info("OAuth validation failed ");
    	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    	result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
		result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNAUTHORIZED);
		result.put(MyAppConstants.MESSAGE, "Invalid OAuth Request");
    	return result;
    }
    
    @RequestMapping(value = "/otherError", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> otherErrorGet(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	Exception exception = (Exception)request.getAttribute("errorMessage");
    	log.error("Error processing request - {}", exception.getMessage());
    	response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    	result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
		result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
		result.put(MyAppConstants.MESSAGE, "There was an error processing your request.");
    	return result;
    }
    
    @RequestMapping(value = "/otherError", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> otherErrorPost(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	Exception exception = (Exception)request.getAttribute("errorMessage");
    	log.error("Error processing request - {}", exception.getMessage());
    	response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    	result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
		result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
		result.put(MyAppConstants.MESSAGE, "There was an error processing your request.");
    	return result;
    }
}
