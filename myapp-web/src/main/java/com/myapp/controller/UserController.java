package com.myapp.controller;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.common.MyAppConstants;
import com.myapp.myapp_service.SubscriptionService;
import com.myapp.myapp_service.UserService;


@Controller
public class UserController {
	
	static final Logger log = LogManager.getLogger(UserController.class.getName());

    @Autowired
    UserService userService;
    
    @Autowired
	SubscriptionService subscriptionService;
    
    @RequestMapping(value = "/assignUser", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> assignUser(@RequestParam("eventUrl") String eventUrl, HttpServletRequest request) {
    	log.info("createSubscription called with eventUrl - {}", eventUrl);
    	return subscriptionService.assignUser(eventUrl, request);
    }
    
    @RequestMapping(value = "/assignUserWithJson", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> assignUserWithJson(@RequestBody ResponseBean responseBean, HttpServletRequest request) {
		return subscriptionService.assignUserWithJson(responseBean, request);
    }
    
    @RequestMapping(value = "/unassignUser", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> unassignUser(@RequestParam("eventUrl") String eventUrl, HttpServletRequest request) {
    	log.info("createSubscription called with eventUrl - {}", eventUrl);
    	return subscriptionService.unassignUser(eventUrl, request);
    }
    
    @RequestMapping(value = "/unassignUserWithJson", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> unassignUserWithJson(@RequestBody ResponseBean responseBean, HttpServletRequest request) {
		return subscriptionService.unassignUserWithJson(responseBean, request);
    }
    
    @RequestMapping(value = "/testPathVariable/{test}", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> testPathVariable(@PathVariable String test, HttpServletRequest request) {
    	log.info("test message is - {}", test);
    	Map<String, Object> result = new HashMap<String, Object>();
    	result.put(MyAppConstants.STATUS, MyAppConstants.SUCCESS);
		result.put(MyAppConstants.RESULT, test);
    	return result;
    }
}
