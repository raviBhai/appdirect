package com.myapp.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myapp.myapp_service.TestService;

@Controller
public class TestController {
	static final Logger log = LogManager.getLogger(TestController.class.getName());
	
	@Autowired
	TestService testService;
	 
	 @RequestMapping(value = "/fetchAllDataFor", method = RequestMethod.POST)
	    public @ResponseBody Map<String, Object> fetchAllDataFor(@RequestParam("object") String object, HttpServletRequest request) {
	    	log.info("getting all data for object - {}", object);
	    	return testService.fetchAllDataFor(object, request);
	    }
}
