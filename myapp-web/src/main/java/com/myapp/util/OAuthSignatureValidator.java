package com.myapp.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.myapp.myapp_dao.common.MyAppConstants;
import com.myapp.myapp_dao.util.PropertyLoader;
import com.ning.http.client.FluentStringsMap;
import com.ning.http.client.oauth.ConsumerKey;
import com.ning.http.client.oauth.OAuthSignatureCalculator;
import com.ning.http.client.oauth.RequestToken;

public class OAuthSignatureValidator {
	static final Logger log = LogManager.getLogger(OAuthSignatureValidator.class.getName());
	
	static String oauthKey = PropertyLoader.getInstance().getValue(MyAppConstants.OAUTH_CONSUMER_PUBLIC_KEY);
	static String oauthSecret = PropertyLoader.getInstance().getValue(MyAppConstants.OAUTH_CONSUMER_SECRET_KEY);
	
	public static boolean isValidOAuthRequest(ServletRequest request) throws UnsupportedEncodingException{
		String method = null;
		String baseURL = null;
		long oauthTimestamp = 0;
		String nonce = null;
		String providedSignature = null;
		FluentStringsMap formParams = null; 
		FluentStringsMap queryParams = null;
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String authorizationHeader = httpRequest.getHeader("Authorization");
		log.info("********authorizationHeader******** is {}", authorizationHeader);
		method = httpRequest.getMethod();
		log.info("********http method******** is {}", method);
		baseURL = httpRequest.getRequestURL().toString();
		log.info("********http baseURL******** is {}", baseURL);
		
		String[] params = authorizationHeader.split(MyAppConstants.COMMA);
		String param;
		for(int i = 0; i < params.length; i++){
			param = params[i];
			String[] subParams = param.split(MyAppConstants.EQUALS_TO);
			String key = subParams[0];
			String value = subParams[1];
			if(param.contains(MyAppConstants.OAUTH_NONCE)){
				nonce = value.substring(1, value.length() - 1);
				log.info("********oauth nonce******** is {}", nonce);
			}
			if(param.contains(MyAppConstants.OAUTH_TIMESTAMP)){
				oauthTimestamp = Long.parseLong(value.substring(1, value.length() - 1));
				log.info("********oauthTimestamp******** is {}", oauthTimestamp);
			}
			if(param.contains(MyAppConstants.OAUTH_CONSUMER_KEY)){
				oauthKey = value.substring(1, value.length() - 1);
				log.info("********oauthKey******** is {}", oauthKey);
			}
			if(param.contains(MyAppConstants.OAUTH_SIGNATURE_METHOD)){
				//nonce = value.substring(1, value.length() - 1);
			}
			if(param.contains(MyAppConstants.OAUTH_VERSION)){
				//nonce = value.substring(1, value.length() - 1);
			}
			if(param.contains(MyAppConstants.OAUTH_SIGNATURE)){
				providedSignature = value.substring(1, value.length() - 1);
				log.info("********oauth providedSignature******** is {}", providedSignature);
			}
			
		}
		log.info("********oauth signatureCalculator******** ");
		OAuthSignatureCalculator signatureCalculator = 
				new OAuthSignatureCalculator(new ConsumerKey(oauthKey, oauthSecret), new RequestToken(null, ""));
		log.info("********oauth signatureCalculator.expectedSignature******** ");
		String expectedSignature = signatureCalculator.calculateSignature(
				method, baseURL, oauthTimestamp, nonce, formParams, queryParams);
		log.info("********oauth expectedSignature******** is {}", expectedSignature);
		
		String decodedSignature = URLDecoder.decode(providedSignature, "UTF-8");
		log.info("********oauth decodedSignature******** is {}", decodedSignature);
		
		if(expectedSignature.equalsIgnoreCase(decodedSignature)){
			log.info("********valid oauth Signature********");
			return true;
		}else{
			log.info("********invalid oauth Signature********");
			return false;
		}
	}
}
