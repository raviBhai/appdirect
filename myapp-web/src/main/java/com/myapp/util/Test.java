package com.myapp.util;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.myapp.myapp_dao.beans.ResponseBean;

public class Test {
	public static void main(String[] args) {
		
		ObjectMapper mapper = new ObjectMapper();
		try {

			
			String json2 = "{ \"type\":\"SUBSCRIPTION_ORDER\", \"marketplace\":{ \"partner\":\"APPDIRECT\", \"baseUrl\":\"https://www.appdirect.com\" }, \"applicationUuid\":null, \"flag\":\"DEVELOPMENT\", \"creator\":{ \"uuid\":\"usr1\", \"openId\":\"https://www.appdirect.com/openid/id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\", \"email\":\"agrawal.ravi1989@gmail.com\", \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"language\":\"en\", \"address\":{ \"salutation\":null, \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"fullName\":\"Ravi Agrawal\", \"companyName\":null, \"phone\":null, \"phoneExtension\":null, \"fax\":null, \"faxExtension\":null, \"street1\":null, \"street2\":null, \"city\":null, \"state\":null, \"zip\":null, \"country\":null, \"pobox\":null, \"pozip\":null }, \"attributes\":null }, \"payload\":{ \"user\":null, \"company\":{ \"uuid\":\"com1\", \"externalId\":null, \"name\":\"Talentica\", \"email\":null, \"phoneNumber\":\"9028128135\", \"website\":\"http://www.talentica.com/\", \"country\":\"US\" }, \"account\":null, \"addonInstance\":null, \"addonBinding\":null, \"order\":{ \"editionCode\":\"FREE\", \"addonOfferingCode\":null, \"pricingDuration\":\"MONTHLY\", \"items\":[] }, \"notice\":null, \"configuration\":{ } }, \"returnUrl\":null, \"links\":[] }";
			
			String assignJson = "{\"type\":\"USER_ASSIGNMENT\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
			
			ResponseBean responseBean = mapper.readValue(assignJson, ResponseBean.class);
			
			// Convert JSON string to Object
			/*String json = "{\"field1\":\"mkyong\",\"field2\":\"ravi\"}";
			Bean user1 = mapper.readValue(json, Bean.class);
			System.out.println(user1.getField1());
			System.out.println(user1.getField2());*/
			
			System.out.println(responseBean);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
