package com.myapp.myapp_service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.common.MyAppConstants;
import com.myapp.myapp_dao.entity.Account;
import com.myapp.myapp_dao.entity.Company;
import com.myapp.myapp_dao.entity.Marketplace;
import com.myapp.myapp_dao.entity.Order;
import com.myapp.myapp_dao.entity.User;
import com.myapp.myapp_dao.enums.AccountStatus;
import com.myapp.utils.Utils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Utils.class})
@PowerMockIgnore("javax.management.*")
public class SubscriptionServiceImplTest {

	@InjectMocks
	private SubscriptionServiceImpl subscriptionServiceImpl = new SubscriptionServiceImpl();
	
	 @Mock
	 UserService userService;
	 
	 @Mock
	 MarketplaceService marketplaceService;
	 
	 @Mock
	 AccountService accountService;
	 
	 @Mock
	 CompanyService companyService;

	 @Mock
	 OrderService orderService;
	 
	 @Test
	 public void createSubscription_userAlreadyExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{ \"type\":\"SUBSCRIPTION_ORDER\", \"marketplace\":{ \"partner\":\"APPDIRECT\", \"baseUrl\":\"https://www.appdirect.com\" }, \"applicationUuid\":null, \"flag\":\"DEVELOPMENT\", \"creator\":{ \"uuid\":\"usr1\", \"openId\":\"https://www.appdirect.com/openid/id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\", \"email\":\"agrawal.ravi1989@gmail.com\", \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"language\":\"en\", \"address\":{ \"salutation\":null, \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"fullName\":\"Ravi Agrawal\", \"companyName\":null, \"phone\":null, \"phoneExtension\":null, \"fax\":null, \"faxExtension\":null, \"street1\":null, \"street2\":null, \"city\":null, \"state\":null, \"zip\":null, \"country\":null, \"pobox\":null, \"pozip\":null }, \"attributes\":null }, \"payload\":{ \"user\":null, \"company\":{ \"uuid\":\"com1\", \"externalId\":null, \"name\":\"Talentica\", \"email\":null, \"phoneNumber\":\"9028128135\", \"website\":\"http://www.talentica.com/\", \"country\":\"US\" }, \"account\":null, \"addonInstance\":null, \"addonBinding\":null, \"order\":{ \"editionCode\":\"FREE\", \"addonOfferingCode\":null, \"pricingDuration\":\"MONTHLY\", \"items\":[] }, \"notice\":null, \"configuration\":{ } }, \"returnUrl\":null, \"links\":[] }";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 User user = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(user);
		 
		 Company company = null;
		 when(companyService.findByUuid(any(String.class))).thenReturn(company);
		 
		 Map<String, Object> result = subscriptionServiceImpl.createSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
		 
	 }
	 
	 @Test
	 public void createSubscription_companyAlreadyExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{ \"type\":\"SUBSCRIPTION_ORDER\", \"marketplace\":{ \"partner\":\"APPDIRECT\", \"baseUrl\":\"https://www.appdirect.com\" }, \"applicationUuid\":null, \"flag\":\"DEVELOPMENT\", \"creator\":{ \"uuid\":\"usr1\", \"openId\":\"https://www.appdirect.com/openid/id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\", \"email\":\"agrawal.ravi1989@gmail.com\", \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"language\":\"en\", \"address\":{ \"salutation\":null, \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"fullName\":\"Ravi Agrawal\", \"companyName\":null, \"phone\":null, \"phoneExtension\":null, \"fax\":null, \"faxExtension\":null, \"street1\":null, \"street2\":null, \"city\":null, \"state\":null, \"zip\":null, \"country\":null, \"pobox\":null, \"pozip\":null }, \"attributes\":null }, \"payload\":{ \"user\":null, \"company\":{ \"uuid\":\"com1\", \"externalId\":null, \"name\":\"Talentica\", \"email\":null, \"phoneNumber\":\"9028128135\", \"website\":\"http://www.talentica.com/\", \"country\":\"US\" }, \"account\":null, \"addonInstance\":null, \"addonBinding\":null, \"order\":{ \"editionCode\":\"FREE\", \"addonOfferingCode\":null, \"pricingDuration\":\"MONTHLY\", \"items\":[] }, \"notice\":null, \"configuration\":{ } }, \"returnUrl\":null, \"links\":[] }";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 User user = null;
		 when(userService.findByUuid(any(String.class))).thenReturn(user);
		 
		 Company company = new Company();
		 when(companyService.findByUuid(any(String.class))).thenReturn(company);
		 
		 Map<String, Object> result = subscriptionServiceImpl.createSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void createSubscription_userAndCompanyAlreadyExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{ \"type\":\"SUBSCRIPTION_ORDER\", \"marketplace\":{ \"partner\":\"APPDIRECT\", \"baseUrl\":\"https://www.appdirect.com\" }, \"applicationUuid\":null, \"flag\":\"DEVELOPMENT\", \"creator\":{ \"uuid\":\"usr1\", \"openId\":\"https://www.appdirect.com/openid/id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\", \"email\":\"agrawal.ravi1989@gmail.com\", \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"language\":\"en\", \"address\":{ \"salutation\":null, \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"fullName\":\"Ravi Agrawal\", \"companyName\":null, \"phone\":null, \"phoneExtension\":null, \"fax\":null, \"faxExtension\":null, \"street1\":null, \"street2\":null, \"city\":null, \"state\":null, \"zip\":null, \"country\":null, \"pobox\":null, \"pozip\":null }, \"attributes\":null }, \"payload\":{ \"user\":null, \"company\":{ \"uuid\":\"com1\", \"externalId\":null, \"name\":\"Talentica\", \"email\":null, \"phoneNumber\":\"9028128135\", \"website\":\"http://www.talentica.com/\", \"country\":\"US\" }, \"account\":null, \"addonInstance\":null, \"addonBinding\":null, \"order\":{ \"editionCode\":\"FREE\", \"addonOfferingCode\":null, \"pricingDuration\":\"MONTHLY\", \"items\":[] }, \"notice\":null, \"configuration\":{ } }, \"returnUrl\":null, \"links\":[] }";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 User user = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(user);
		 
		 Company company = new Company();
		 when(companyService.findByUuid(any(String.class))).thenReturn(company);
		 
		 Map<String, Object> result = subscriptionServiceImpl.createSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void createSubscription_userAndCompanyDoesNotExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{ \"type\":\"SUBSCRIPTION_ORDER\", \"marketplace\":{ \"partner\":\"APPDIRECT\", \"baseUrl\":\"https://www.appdirect.com\" }, \"applicationUuid\":null, \"flag\":\"DEVELOPMENT\", \"creator\":{ \"uuid\":\"usr1\", \"openId\":\"https://www.appdirect.com/openid/id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\", \"email\":\"agrawal.ravi1989@gmail.com\", \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"language\":\"en\", \"address\":{ \"salutation\":null, \"firstName\":\"Ravi\", \"lastName\":\"Agrawal\", \"fullName\":\"Ravi Agrawal\", \"companyName\":null, \"phone\":null, \"phoneExtension\":null, \"fax\":null, \"faxExtension\":null, \"street1\":null, \"street2\":null, \"city\":null, \"state\":null, \"zip\":null, \"country\":null, \"pobox\":null, \"pozip\":null }, \"attributes\":null }, \"payload\":{ \"user\":null, \"company\":{ \"uuid\":\"com1\", \"externalId\":null, \"name\":\"Talentica\", \"email\":null, \"phoneNumber\":\"9028128135\", \"website\":\"http://www.talentica.com/\", \"country\":\"US\" }, \"account\":null, \"addonInstance\":null, \"addonBinding\":null, \"order\":{ \"editionCode\":\"FREE\", \"addonOfferingCode\":null, \"pricingDuration\":\"MONTHLY\", \"items\":[] }, \"notice\":null, \"configuration\":{ } }, \"returnUrl\":null, \"links\":[] }";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 User user = null;
		 when(userService.findByUuid(any(String.class))).thenReturn(user);
		 
		 Company company = null;
		 when(companyService.findByUuid(any(String.class))).thenReturn(company);
		 
		 doNothing().when(companyService).save(any(Company.class));
		 doNothing().when(marketplaceService).save(any(Marketplace.class));
		 doNothing().when(accountService).save(any(Account.class));
		 doNothing().when(userService).save(any(User.class));
		 doNothing().when(accountService).update(any(Account.class));
		 doNothing().when(orderService).save(any(Order.class));
		 
		 Map<String, Object> result = subscriptionServiceImpl.createSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.TRUE));
		 assertTrue(!result.get(MyAppConstants.ACCOUNT_IDENTIFIER).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void cancelSubscription_accountIsNull(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 
		 Account account = null;
		 when(accountService.findByUuid(any(String.class))).thenReturn(account);
		 Map<String, Object> result = subscriptionServiceImpl.cancelSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void cancelSubscription_userPermissionDenied(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid("test uuid");
		 account.setAdminUser(adminUser);
		 when(accountService.findByUuid(any(String.class))).thenReturn(account);
		 Map<String, Object> result = subscriptionServiceImpl.cancelSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void cancelSubscription_alreadyCancelled(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 
		 String eventCreatorUuid = User.getCreatorUuidFromResponseBean(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid(eventCreatorUuid);
		 account.setAdminUser(adminUser);
		 account.setStatus(AccountStatus.CANCELLED);
		 when(accountService.findByUuid(any(String.class))).thenReturn(account);
		 Map<String, Object> result = subscriptionServiceImpl.cancelSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void cancelSubscription_cancelSubscription(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 
		 String eventCreatorUuid = User.getCreatorUuidFromResponseBean(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid(eventCreatorUuid);
		 account.setAdminUser(adminUser);
		 when(accountService.findByUuid(any(String.class))).thenReturn(account);
		 doNothing().when(accountService).update(any(Account.class));
		 Map<String, Object> result = subscriptionServiceImpl.cancelSubscription(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.TRUE));
	 }
	 
	 @Test
	 public void assignUser_accountDoesNotExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = null;
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 Map<String, Object> result = subscriptionServiceImpl.assignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
		 
	 }
	 
	 @Test
	 public void assignUser_creatorDoesNotExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = null;
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 
		 Map<String, Object> result = subscriptionServiceImpl.assignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
		 
	 }
	 
	 @Test
	 public void assignUser_creatorIsNotAdmin(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid("dummy uuid");
		 account.setAdminUser(adminUser);
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 
		 Map<String, Object> result = subscriptionServiceImpl.assignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void assignUser_userAlreadyPresent(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid(" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d");
		 account.setAdminUser(adminUser);
		 User user2 = new User();
		 user2.setUuid("11d7bde5-93 71-4293-b48b-a98651076850");
		 List<User> existingUsers = new ArrayList<User>();
		 existingUsers.add(user2);
		 account.setUsers(existingUsers);
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 
		 Map<String, Object> result = subscriptionServiceImpl.assignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void assignUser_assignedUser(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid(" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d");
		 account.setAdminUser(adminUser);
		 User user2 = new User();
		 user2.setUuid("user2 dummy uuid");
		 List<User> existingUsers = new ArrayList<User>();
		 existingUsers.add(user2);
		 account.setUsers(existingUsers);
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 doNothing().when(userService).save(any(User.class));
		 doNothing().when(accountService).update(any(Account.class));
		 
		 Map<String, Object> result = subscriptionServiceImpl.assignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.TRUE));
	 }
	 
	 @Test
	 public void unassignUser_accountDoesNotExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = null;
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 Map<String, Object> result = subscriptionServiceImpl.unassignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
		 
	 }
	 
	 @Test
	 public void unassignUser_creatorDoesNotExists(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = null;
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 
		 Map<String, Object> result = subscriptionServiceImpl.unassignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
		 
	 }
	 
	 @Test
	 public void unassignUser_creatorIsNotAdmin(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid("dummy uuid");
		 account.setAdminUser(adminUser);
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 
		 Map<String, Object> result = subscriptionServiceImpl.unassignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void unassignUser_userNotPresent(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid(" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d");
		 account.setAdminUser(adminUser);
		 User user2 = new User();
		 user2.setUuid("user2 dummy uuid");
		 List<User> existingUsers = new ArrayList<User>();
		 existingUsers.add(user2);
		 account.setUsers(existingUsers);
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 
		 Map<String, Object> result = subscriptionServiceImpl.unassignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.FALSE));
		 assertTrue(!result.get(MyAppConstants.ERROR_CODE).equals(MyAppConstants.EMPTY_STRING));
		 assertTrue(!result.get(MyAppConstants.MESSAGE).equals(MyAppConstants.EMPTY_STRING));
	 }
	 
	 @Test
	 public void unassignUser_unassignedUser(){
		 PowerMockito.mockStatic(Utils.class);
		 HttpServletRequest  request = Mockito.mock(HttpServletRequest.class);
		 String eventUrl = "https://www.google.co.in";
		 String eventDataInJson = "{\"type\":\"SUBSCRIPTION_CANCEL\",\"marketplace\":{\"partner\":\"APPDIRECT\",\"baseUrl\":\"https://www .appdirect.com\"},\"applicationUuid\":null,\"flag\":\"DEVELOPMENT\",\"creator\":{\"uuid\":\" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"openId\":\"https://www.appdirect.com/openid /id/9bf0a97e-6ac2-4717-a5b8-3bbe915f144d\",\"email\":\"agrawal.ravi1989@gmail.com\",\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":{\"salutation\":null,\"firstName\":\"Ravi\",\"lastName\":\"Agrawal\",\"fullName\":\"Ravi Agrawal\",\"companyName\":null,\"phone\":null,\"phoneExtension\":null,\"fax\":null,\"faxExtension\":null,\"street1\":null,\"street2\":null,\"city\":null,\"state\":null,\"zip\":null,\"country\":null,\"pobox\":null,\"pozip\":null},\"attributes\":null},\"payload\":{\"user\":{\"uuid\":\"11d7bde5-93 71-4293-b48b-a98651076850\",\"openId\":\"https://www.appdirect.com/openid/id/11d7bde 5-9371-4293-b48b-a98651076850\",\"email\":\"ravi.agrawal@talentica.com\",\"firstName\": \"Ravi\",\"lastName\":\"Agrawal\",\"language\":\"en\",\"address\":null,\"attributes\":null},\"company\":null,\"account\":{\"accountIdentifier\":\"2f533335-4fb2-46ca-82d1-8b2690037eb 1\",\"status\":\"ACTIVE\",\"parentAccountIdentifier\":null},\"addonInstance\":null,\"addonBinding\":null,\"order\":null,\"notice\":null,\"configuration\":{}},\"returnUrl\":null,\"links\":[]}";
		 ResponseBean responseBean = getResponseBeanFromJson(eventDataInJson);;
		 PowerMockito.when(Utils.getJsonDataFromEventUrl(eventUrl)).thenReturn(eventDataInJson);
		 PowerMockito.when(Utils.getResponseBeanFromJson(eventDataInJson)).thenReturn(responseBean);
		 Account account = new Account();
		 User adminUser = new User();
		 adminUser.setUuid(" 9bf0a97e-6ac2-4717-a5b8-3bbe915f144d");
		 account.setAdminUser(adminUser);
		 User user2 = new User();
		 user2.setUuid("11d7bde5-93 71-4293-b48b-a98651076850");
		 List<User> existingUsers = new ArrayList<User>();
		 existingUsers.add(user2);
		 account.setUsers(existingUsers);
		 when(accountService.findByUuidWithAllUsers(any(String.class))).thenReturn(account);
		 
		 User creator = new User();
		 when(userService.findByUuid(any(String.class))).thenReturn(creator);
		 doNothing().when(userService).save(any(User.class));
		 doNothing().when(accountService).update(any(Account.class));
		 
		 Map<String, Object> result = subscriptionServiceImpl.unassignUser(eventUrl, request);
		 assertTrue(result.get(MyAppConstants.SUCCESS).equals(MyAppConstants.TRUE));
	 }
	 
	 public static ResponseBean getResponseBeanFromJson(String responseJson){
			ResponseBean responseBean = null;
			ObjectMapper mapper = new ObjectMapper();
			try {
				responseBean = mapper.readValue(responseJson, ResponseBean.class);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return responseBean;
		}
}
