package com.myapp.bean;

import com.myapp.myapp_dao.enums.UserStatusType;


public class UserDTO {
	private int userId;
	private String name;
	private String about;
	private UserStatusType status;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public UserStatusType getStatus() {
		return status;
	}
	public void setStatus(UserStatusType status) {
		this.status = status;
	}
	

}
