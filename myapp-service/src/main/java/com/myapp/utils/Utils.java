package com.myapp.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.common.MyAppConstants;
import com.myapp.myapp_dao.util.PropertyLoader;

public class Utils {
	
	static final Logger log = LogManager.getLogger(Utils.class.getName());

	public static String getJsonDataFromEventUrl(String eventUrl){
		String responseJson = "";
		try{
			OAuthConsumer consumer = new DefaultOAuthConsumer(
					PropertyLoader.getInstance().getValue(MyAppConstants.OAUTH_CONSUMER_PUBLIC_KEY), 
					PropertyLoader.getInstance().getValue(MyAppConstants.OAUTH_CONSUMER_SECRET_KEY));
			URL url = new URL(eventUrl);
			HttpURLConnection request = (HttpURLConnection) url.openConnection();
			request.setRequestProperty("Accept", "application/json");
			consumer.sign(request);
			
			int responseCode = request.getResponseCode();
			log.info("Sending 'GET' request to URL : {}", eventUrl);
			log.info("Response Code : {}", responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			responseJson = response.toString();
			log.info("Response json from app direct server is - {}", responseJson);
			//request.connect();
		}catch(Exception e){
			log.error("Error fetching data from eventUrl - {}", e.getMessage());
		}
		return responseJson;
	}
	
	public static ResponseBean getResponseBeanFromJson(String responseJson){
		ResponseBean responseBean = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			log.info("In getResponseBeanFromJson");
			responseBean = mapper.readValue(responseJson, ResponseBean.class);
			log.info("getResponseBeanFromJson - json converted to bean");
		} catch (JsonGenerationException e) {
			log.info("Error while parsing json to object with reason - {}", e.getMessage());
		} catch (JsonMappingException e) {
			log.info("Error while parsing json to object with reason - {}", e.getMessage());
		} catch (IOException e) {
			log.info("Error while parsing json to object with reason - {}", e.getMessage());
		}
		return responseBean;
	}
}
