package com.myapp.exceptions;

import com.myapp.myapp_dao.common.ErrorCodes;

public class AccountNotFoundException extends CustomParentException{

	private static final long serialVersionUID = 1L;
	
 	public AccountNotFoundException(String message, Throwable cause){
        super(message, cause);
    }

    public AccountNotFoundException(String message){
        super(message, ErrorCodes.ACCOUNT_NOT_FOUND);
    }
    
    public AccountNotFoundException(){
    	
    }
}
