package com.myapp.exceptions;

import com.myapp.myapp_dao.common.ErrorCodes;

public class ForbiddenException extends CustomParentException{

	private static final long serialVersionUID = 1L;
	
 	public ForbiddenException(String message, Throwable cause){
        super(message, cause);
    }

    public ForbiddenException(String message){
    	super(message, ErrorCodes.FORBIDDEN);
    }
    
    public ForbiddenException(){
    	
    }
}
