package com.myapp.exceptions;

import com.myapp.myapp_dao.common.ErrorCodes;

public class UserNotFoundException extends CustomParentException{

	private static final long serialVersionUID = 1L;
	
 	public UserNotFoundException(String message, Throwable cause){
        super(message, cause);
    }

    public UserNotFoundException(String message){
    	super(message, ErrorCodes.USER_NOT_FOUND);
    }
    
    public UserNotFoundException(){
    	
    }

}
