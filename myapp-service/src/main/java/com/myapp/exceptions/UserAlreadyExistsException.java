package com.myapp.exceptions;

import com.myapp.myapp_dao.common.ErrorCodes;

public class UserAlreadyExistsException extends CustomParentException{
	
	private static final long serialVersionUID = 1L;
	
	public UserAlreadyExistsException(String message, Throwable cause){
		super(message, cause);
	}
	
	 public UserAlreadyExistsException(String message){
		 super(message, ErrorCodes.USER_ALREADY_EXISTS);
	 }
	 
	 public UserAlreadyExistsException(){
		 
	 }

}
