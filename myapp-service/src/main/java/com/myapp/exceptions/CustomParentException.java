package com.myapp.exceptions;


public class CustomParentException extends Exception{

	protected String errorCode;
	protected String message;
	
 	public CustomParentException(String message, Throwable cause){
        super(message, cause);
    }

    public CustomParentException(String message, String errorCode){
        super(message);
        this.message = message;
        this.errorCode = errorCode;
    }
    
    public CustomParentException(){
    	
    }

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
    
    
}
