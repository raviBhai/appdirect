package com.myapp.myapp_service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.myapp_dao.common.MyAppConstants;

@Service
public class TestServiceImpl implements TestService{
	
	 @Autowired
	 UserService userService;
	 
	 @Autowired
	 MarketplaceService marketplaceService;
	 
	 @Autowired
	 AccountService accountService;
	 
	 @Autowired
	 CompanyService companyService;

	 @Autowired
	 OrderService orderService;
	 
	public Map<String, Object> fetchAllDataFor(String object, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
		if(object.equalsIgnoreCase("company")){
			result.put(MyAppConstants.RESULT, companyService.findAll());
		}else if(object.equalsIgnoreCase("account")){
			result.put(MyAppConstants.RESULT, accountService.findAll());
		}else if(object.equalsIgnoreCase("marketplace")){
			result.put(MyAppConstants.RESULT, marketplaceService.findAll());
		}else if(object.equalsIgnoreCase("user")){
			result.put(MyAppConstants.RESULT, userService.findAll());
		}else if(object.equalsIgnoreCase("order")){
			result.put(MyAppConstants.RESULT, orderService.findAll());
		}
		return result;
	}
}
