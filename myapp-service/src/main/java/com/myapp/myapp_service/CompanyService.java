package com.myapp.myapp_service;

import java.util.List;

import com.myapp.myapp_dao.entity.Company;


public interface CompanyService {
	
	public void save(Company company);
	
	public void saveWithFlush(Company company);
	
	public List<Company> findAll();
	
	public Company findByUuid(String uuid);
}
