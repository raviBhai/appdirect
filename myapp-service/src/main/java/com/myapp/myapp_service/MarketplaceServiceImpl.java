package com.myapp.myapp_service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.myapp_dao.MarketplaceDao;
import com.myapp.myapp_dao.entity.Marketplace;

@Service
public class MarketplaceServiceImpl implements MarketplaceService{

	@Autowired
	MarketplaceDao marketplaceDao;

	public void save(Marketplace marketplace) {
		marketplaceDao.save(marketplace);
	}

	public void saveWithFlush(Marketplace marketplace) {
		marketplaceDao.save(marketplace);
		marketplaceDao.flush();
	}

	public List<Marketplace> findAll() {
		return marketplaceDao.findAll();
	}
	
	
}
