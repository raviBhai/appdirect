package com.myapp.myapp_service;

import java.util.List;

import com.myapp.myapp_dao.entity.Account;


public interface AccountService {
	
	public void save(Account account);
	
	public void saveWithFlush(Account account);
	
	public void update(Account account);
	
	public Account findByUuid(String accountIdentifier);
	
	public Account findByUuidWithAllUsers(String accountIdentifier);
	
	public void cancelSubscription(String accountUuid);
	
	public List<Account> findAll();
}
