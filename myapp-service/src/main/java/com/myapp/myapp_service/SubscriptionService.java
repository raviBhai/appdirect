package com.myapp.myapp_service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.myapp.myapp_dao.beans.ResponseBean;


public interface SubscriptionService {
	
	public Map<String, Object> createSubscription(String eventUrl, HttpServletRequest request);
	
	public Map<String, Object> cancelSubscription(String eventUrl, HttpServletRequest request);
	
	public Map<String, Object> createSubscriptionWithJson(ResponseBean responseBean, HttpServletRequest request);
	
	public Map<String, Object> cancelSubscriptionWithJson(ResponseBean responseBean, HttpServletRequest request);
	
	public Map<String, Object> assignUser(String eventUrl, HttpServletRequest request);
	
	public Map<String, Object> assignUserWithJson(ResponseBean responseBean, HttpServletRequest request);
	
	public Map<String, Object> unassignUser(String eventUrl, HttpServletRequest request);
	
	public Map<String, Object> unassignUserWithJson(ResponseBean responseBean, HttpServletRequest request);
	
}
