package com.myapp.myapp_service;

import java.util.List;

import com.myapp.myapp_dao.entity.User;

public interface UserService {
	
	public void save(User user);
	
	public void saveWtihFlush(User user);
	
	public User findByUuid(String uuid);
	
	public List<User> findAll();

}
