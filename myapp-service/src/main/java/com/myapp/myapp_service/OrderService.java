package com.myapp.myapp_service;

import java.util.List;

import com.myapp.myapp_dao.entity.Order;


public interface OrderService {
	
	public void save(Order order);
	
	public void saveWithflush(Order order);
	
	public List<Order> findAll();
}
