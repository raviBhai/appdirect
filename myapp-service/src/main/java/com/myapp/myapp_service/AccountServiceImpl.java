package com.myapp.myapp_service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.myapp_dao.AccountDao;
import com.myapp.myapp_dao.entity.Account;
import com.myapp.myapp_dao.enums.AccountStatus;

@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	AccountDao accountDao;

	public void save(Account account) {
		accountDao.save(account);
	}

	public void cancelSubscription(String accountUuid) {
		Account account = accountDao.findByUuid(accountUuid);
		account.setStatus(AccountStatus.CANCELLED);
		accountDao.update(account);
	}

	public void saveWithFlush(Account account) {
		accountDao.save(account);
		accountDao.flush();
	}

	public Account findByUuid(String accountIdentifier) {
		return accountDao.findByUuid(accountIdentifier);
	}

	public void update(Account account) {
		accountDao.update(account);
	}

	public List<Account> findAll() {
		return accountDao.findAll();
	}

	public Account findByUuidWithAllUsers(String accountIdentifier) {
		return accountDao.findByUuidWithAllUsers(accountIdentifier);
	}
	
}
