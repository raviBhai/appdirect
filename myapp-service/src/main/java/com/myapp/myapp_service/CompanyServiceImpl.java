package com.myapp.myapp_service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.myapp_dao.CompanyDao;
import com.myapp.myapp_dao.entity.Company;

@Service
public class CompanyServiceImpl implements CompanyService{

	@Autowired
	CompanyDao companyDao;

	public void save(Company company) {
		companyDao.save(company);
	}

	public void saveWithFlush(Company company) {
		companyDao.save(company);
		companyDao.flush();
	}

	public List<Company> findAll() {
		return companyDao.findAll();
	}
	
	public Company findByUuid(String uuid) {
		return companyDao.findByUuid(uuid);
	}
	
}
