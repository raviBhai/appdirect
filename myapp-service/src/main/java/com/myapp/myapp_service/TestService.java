package com.myapp.myapp_service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;


public interface TestService {
	
	public Map<String, Object> fetchAllDataFor(String object, HttpServletRequest request);
	
}
