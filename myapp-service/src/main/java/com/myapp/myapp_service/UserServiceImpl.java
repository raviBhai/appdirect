package com.myapp.myapp_service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.myapp_dao.UserDao;
import com.myapp.myapp_dao.entity.User;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserDao userDao;

	public void save(User user){
		userDao.save(user);
	}

	public User findByUuid(String uuid) {
		return userDao.findByUuid(uuid);
	}

	public void saveWtihFlush(User user) {
		userDao.save(user);
		userDao.flush();
	}

	public List<User> findAll() {
		return userDao.findAll();
	}
}
