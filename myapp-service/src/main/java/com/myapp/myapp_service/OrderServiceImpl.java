package com.myapp.myapp_service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.myapp_dao.OrderDao;
import com.myapp.myapp_dao.entity.Order;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	OrderDao orderDao;

	public void save(Order order) {
		orderDao.save(order);
	}

	public void saveWithflush(Order order) {
		orderDao.save(order);
		orderDao.flush();
	}

	public List<Order> findAll() {
		return orderDao.findAll();
	}
}
