package com.myapp.myapp_service;

import java.util.List;

import com.myapp.myapp_dao.entity.Marketplace;


public interface MarketplaceService {
	
	public void save(Marketplace marketplace);
	
	public void saveWithFlush(Marketplace marketplace);
	
	public List<Marketplace> findAll();
}
