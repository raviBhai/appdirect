package com.myapp.myapp_service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myapp.exceptions.AccountNotFoundException;
import com.myapp.exceptions.CustomParentException;
import com.myapp.exceptions.ForbiddenException;
import com.myapp.exceptions.UserAlreadyExistsException;
import com.myapp.exceptions.UserNotFoundException;
import com.myapp.myapp_dao.beans.ResponseBean;
import com.myapp.myapp_dao.common.ErrorCodes;
import com.myapp.myapp_dao.common.MyAppConstants;
import com.myapp.myapp_dao.entity.Account;
import com.myapp.myapp_dao.entity.Company;
import com.myapp.myapp_dao.entity.Marketplace;
import com.myapp.myapp_dao.entity.Order;
import com.myapp.myapp_dao.entity.User;
import com.myapp.myapp_dao.enums.AccountStatus;
import com.myapp.myapp_dao.enums.UserStatusType;
import com.myapp.myapp_dao.util.CommonUtil;
import com.myapp.utils.Utils;

@Service
public class SubscriptionServiceImpl implements SubscriptionService{
	static final Logger log = LogManager.getLogger(SubscriptionServiceImpl.class.getName());
	
	 @Autowired
	 UserService userService;
	 
	 @Autowired
	 MarketplaceService marketplaceService;
	 
	 @Autowired
	 AccountService accountService;
	 
	 @Autowired
	 CompanyService companyService;

	 @Autowired
	 OrderService orderService;
	 
	@Transactional
	public Map<String, Object> createSubscription(String eventUrl, HttpServletRequest request) {
		log.info("Entering createSubscription");
		Map<String, Object> result = new HashMap<String, Object>();
		String eventDataInJson = Utils.getJsonDataFromEventUrl(eventUrl);
		ResponseBean responseBean = Utils.getResponseBeanFromJson(eventDataInJson);
		log.info("createSubscription - obtained response bean");
		try{
			Account account = createAccount(responseBean);
			log.info("createSubscription - account created with identifier - {}", account.getAccountIdentifier());
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
			result.put(MyAppConstants.ACCOUNT_IDENTIFIER, account.getAccountIdentifier());
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}

	@Transactional
	public Map<String, Object> cancelSubscription(String eventUrl, HttpServletRequest request) {
		log.info("Entering cancelSubscription");
		Map<String, Object> result = new HashMap<String, Object>();
		String eventDataInJson = Utils.getJsonDataFromEventUrl(eventUrl);
		ResponseBean responseBean = Utils.getResponseBeanFromJson(eventDataInJson);
		log.info("cancelSubscription - obtained response bean");
		try{
			cancelSubscription(responseBean);
			log.info("cancelSubscription - cancelled for given account");
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}
	
	@Transactional
	public Map<String, Object> createSubscriptionWithJson(ResponseBean responseBean, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		try{
			Account account = createAccount(responseBean);
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
			result.put(MyAppConstants.ACCOUNT_IDENTIFIER, account.getAccountIdentifier());
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}

	@Transactional
	public Map<String, Object> cancelSubscriptionWithJson(ResponseBean responseBean, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		try{
			cancelSubscription(responseBean);
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}
	
	@Transactional
	public Map<String, Object> assignUser(String eventUrl, HttpServletRequest request) {
		log.info("Entering assignUser");
		Map<String, Object> result = new HashMap<String, Object>();
		String eventDataInJson = Utils.getJsonDataFromEventUrl(eventUrl);
		ResponseBean responseBean = Utils.getResponseBeanFromJson(eventDataInJson);
		log.info("assignUser - obtained response bean");
		try{
			assignUserToAccount(responseBean);
			log.info("assignUser - successfully assigned user");
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}

	@Transactional
	public Map<String, Object> assignUserWithJson(ResponseBean responseBean, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		try{
			assignUserToAccount(responseBean);
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}
	
	@Transactional
	public Map<String, Object> unassignUser(String eventUrl, HttpServletRequest request) {
		log.info("Entering unassignUser");
		Map<String, Object> result = new HashMap<String, Object>();
		String eventDataInJson = Utils.getJsonDataFromEventUrl(eventUrl);
		ResponseBean responseBean = Utils.getResponseBeanFromJson(eventDataInJson);
		log.info("unassignUser - obtained response bean");
		try{
			unassignUserFromAccount(responseBean);
			log.info("unassignUser - successfully unassigned user");
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}

	@Transactional
	public Map<String, Object> unassignUserWithJson(ResponseBean responseBean, HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		try{
			unassignUserFromAccount(responseBean);
			result.put(MyAppConstants.SUCCESS, MyAppConstants.TRUE);
		}catch (CustomParentException e){
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, e.getErrorCode());
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			result.put(MyAppConstants.SUCCESS, MyAppConstants.FALSE);
			result.put(MyAppConstants.ERROR_CODE, ErrorCodes.UNKNOWN_ERROR);
			result.put(MyAppConstants.MESSAGE, e.getMessage());
		}
		return result;
	}
	
	public Account createAccount(ResponseBean responseBean) throws CustomParentException {
		log.info("Entering createAccount");
		User existingUser = getUserByUuidFromResponseBean(responseBean); //find user by creator uuid from responseBean
		Company existingCompany = getCompanyByUuidFromResponseBean(responseBean);
		if(CommonUtil.isNull(existingUser) && CommonUtil.isNull(existingCompany)){
			log.info("createAccount - no existing account. About to create new account");
			Company company = Company.getCompanyFromResponseBean(responseBean);
			Marketplace marketplace = Marketplace.getMarketplaceFromResponseBean(responseBean);
			Account account = Account.createNewAccount();
			User user = User.getCreatorUserFromResponseBean(responseBean);		
			Order order = Order.getOrderFromResponseBean(responseBean);
			
			createCompany(company);
			createMarketPlace(marketplace);
			createAccountUnderCompanyOnMarketplace(company, marketplace, account);
			createUserInAccount(account, user);
			assignAdminToAccount(account, user);
			createOrderByUser(user, order);
			log.info("createAccount - created new account");
			return account;
		}else{
			//account subscription already exists
			log.info("createAccount - account already exists");
			throw new UserAlreadyExistsException("An account has already been created by this user or company.");
		}
	}

	public void cancelSubscription(ResponseBean responseBean) throws CustomParentException{
		log.info("Entering cancelSubscription");
		String accountIdentifier = Account.getAccountIdentifierFromResponseBean(responseBean); 
		Account account = accountService.findByUuid(accountIdentifier);
		if(CommonUtil.isNull(account) ){
			//account does not exists
			log.info("cancelSubscription - account to cancel subscription not found");
			throw new AccountNotFoundException("The account for which yor are trying to cancel the subscription does not exists.");
		}
		
		String eventCreatorUuid = User.getCreatorUuidFromResponseBean(responseBean);
		User adminUser = account.getAdminUser();
		if(eventCreatorUuid.equals(adminUser.getUuid())){
			if(account.getStatus() == AccountStatus.CANCELLED){
				//account is already cancelled
				log.info("cancelSubscription - subscription is already cancelled");
				throw new AccountNotFoundException("Subscription is already cancelled for this account.");
			}else{
				log.info("cancelSubscription - about to cancel subscription");
				account.setStatus(AccountStatus.CANCELLED);
				accountService.update(account);
				log.info("cancelSubscription - subscription cancelled successfully");
			}
		}else{
			log.info("cancelSubscription - creator user is not admin of given account. Hence cannot cancel subscription");
			throw new ForbiddenException("You are not allowed to cancel the subscription for this account.");
		}
	}
	
	public void assignUserToAccount(ResponseBean responseBean) throws CustomParentException{
		log.info("Entering assignUserToAccount");
		String accountIdentifier = Account.getAccountIdentifierFromResponseBean(responseBean); 
		String creatorUuid = User.getCreatorUuidFromResponseBean(responseBean);
		Account account = accountService.findByUuidWithAllUsers(accountIdentifier);
		if(CommonUtil.isNull(account)){
			log.info("assignUserToAccount - account to assign user does not exists");
			throw new AccountNotFoundException("The account to which you are trying to assign the user does not exists.");
		}
		User creator = userService.findByUuid(creatorUuid);
		if(CommonUtil.isNull(creator)){
			log.info("assignUserToAccount - event creator user does not exists");
			throw new UserNotFoundException("The creator user does not exists.");
		}
		User adminUser = account.getAdminUser();
		if(creatorUuid.equals(adminUser.getUuid())){
			log.info("assignUserToAccount - creator user is admin of the account to which new user is to be added");
			List<User> accountUsers = account.getUsers();
			User payloadUser = User.getPayloadUserFromResponseBean(responseBean);
			boolean isAlreadyAdded = isPayloadUserAlreadyAddedInTheAccount(accountUsers, payloadUser);
			if(!isAlreadyAdded){
				addNewUserToTheAccount(account, payloadUser);
			}
		}else{
			log.info("assignUserToAccount - event creator user is not admin of given account. Hence cannot add new user to account");
			throw new ForbiddenException("You are not allowed to add a user to this account.");
		}
	}
	
	public void unassignUserFromAccount(ResponseBean responseBean) throws CustomParentException{
		log.info("Entering unassignUserFromAccount");
		String accountIdentifier = Account.getAccountIdentifierFromResponseBean(responseBean); 
		String creatorUuid = User.getCreatorUuidFromResponseBean(responseBean);
		Account account = accountService.findByUuidWithAllUsers(accountIdentifier);
		if(CommonUtil.isNull(account)){
			log.info("unassignUserFromAccount - account from which user needs to be  unassigned does not exists");
			throw new AccountNotFoundException("The account from which you are trying to unassign the user does not exists.");
		}
		User creator = userService.findByUuid(creatorUuid);
		if(CommonUtil.isNull(creator)){
			log.info("unassignUserFromAccount - event creator user does not exists");
			throw new UserNotFoundException("The creator user does not exists.");
		}
		User adminUser = account.getAdminUser();
		if(creatorUuid.equals(adminUser.getUuid())){
			log.info("unassignUserFromAccount - creator user is admin of the account from which payload user is to be removed");
			List<User> accountUsers = account.getUsers();
			User payloadUser = User.getPayloadUserFromResponseBean(responseBean);
			boolean isUserPresent = unassignUserFromTheAccount(accountUsers, payloadUser);
			if(!isUserPresent){
				throw new UserNotFoundException("The user which needs to be unassigned does not exists.");
			}
		}else{
			log.info("unassignUserFromAccount - event creator user is not admin of given account. Hence cannot unassign user from given account");
			throw new ForbiddenException("You are not allowed to unassign user from this account.");
		}
	}

	private boolean unassignUserFromTheAccount(List<User> accountUsers, User payloadUser) {
		boolean isUserPresent = false;
		for (User user : accountUsers) {
			if(user.getUuid().equals(payloadUser.getUuid())){
				isUserPresent = true;
				log.info("unassignUserFromAccount - found user which needs to be unassigned");
				user.setStatus(UserStatusType.DELETED);
				userService.save(user);
				break;
			}
		}
		return isUserPresent;
	}

	private boolean isPayloadUserAlreadyAddedInTheAccount(List<User> accountUsers, User payloadUser) throws UserAlreadyExistsException {
		boolean isAlreadyAdded = false;
		for (User user : accountUsers) {
			if(user.getUuid().equals(payloadUser.getUuid())){
				isAlreadyAdded = true;
				log.info("assignUserToAccount - payload new user is already added in the account");
				throw new UserAlreadyExistsException("The user you are trying to add already exists in the account.");
			}
		}
		return isAlreadyAdded;
	}

	private void addNewUserToTheAccount(Account account, User payloadUser) {
		log.info("assignUserToAccount - about to add new user in the account");
		payloadUser.setAccount(account);
		userService.save(payloadUser);
		account.addUser(payloadUser); 
		accountService.update(account);
		log.info("assignUserToAccount - successfully added new user in the account");
	}
	
	private void createOrderByUser(User user, Order order) {
		//create order
		order.setCreator(user);
		orderService.save(order);
	}

	private void createUserInAccount(Account account, User user) {
		//create a user with account
		user.setAccount(account);
		userService.save(user);
	}
	
	public void assignAdminToAccount(Account account, User user) {
		//assign admin to account
		account.setAdminUser(user);
		accountService.update(account);
	}

	private void createAccountUnderCompanyOnMarketplace(Company company,
			Marketplace marketplace, Account account) {
		//create an account with market place and company
		account.setMarketplace(marketplace);
		account.setCompany(company);
		accountService.save(account);
	}

	private void createMarketPlace(Marketplace marketplace) {
		//create a market place
		marketplaceService.save(marketplace);
	}

	private void createCompany(Company company) {
		//create a company
		companyService.save(company);
	}
	
	private User getUserByUuidFromResponseBean(ResponseBean responseBean){
		String uuid = User.getCreatorUuidFromResponseBean(responseBean);
		log.info("getUserByUuidFromResponseBean() called and uuid obtained is  {}", uuid);
		User user = userService.findByUuid(uuid);
		return user;
	}
	
	private Company getCompanyByUuidFromResponseBean(ResponseBean responseBean){
		String uuid = Company.getUuidFromResponseBean(responseBean);
		log.info("getCompanyByUuidFromResponseBean() called and uuid obtained is  {}", uuid);
		Company company = companyService.findByUuid(uuid);
		return company;
	}

}
